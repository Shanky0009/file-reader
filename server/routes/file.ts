import { Request, Response, Router } from "express";
import * as multer from 'multer';
import * as textract from "textract";
import * as fs from 'fs';

const fileRouter: Router = Router();

var DIR = './server/uploads/';

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './server/uploads/')
  },
  filename: function (req, file, cb) {
    let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
    cb(null, Date.now() + ext) //Appending .jpg
  }
});

var upload = multer({storage: storage}).single('file');

fileRouter.get("/", (request: Request, response: Response) => {
  response.json("upload a file");
});

fileRouter.post("/", upload, (request: Request, response: Response) => {
  textract.fromFileWithPath(process.cwd()+'/'+request.file.path, function( error, text ) {
    var words = text.match(/[^[\]]+(?=])/g)
    response.json(words);
    fs.unlinkSync(process.cwd()+'/'+request.file.path)
    console.log( words );
  });
});

export { fileRouter };
