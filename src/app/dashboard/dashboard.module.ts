import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'ng2-file-upload';

import { DashboardComponent } from './dashboard.component';
import { routes } from './dashboard.router';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FileUploadModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DashboardComponent
  ],
  bootstrap: [
    DashboardComponent
  ]
})
export class DashboardModule {}
