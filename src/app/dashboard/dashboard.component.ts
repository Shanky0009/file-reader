import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { FileUploader } from 'ng2-file-upload';

const URL = 'http://localhost:4300/api/file';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent {
  words: any;
  public uploader:FileUploader = new FileUploader({url: URL, autoUpload: true});

  constructor() {
    this.uploader.onSuccessItem= (item: any, response: any, status: any, headers: any) => {
        console.log('Upload complete status->' + status);
        console.log('Upload complete header->' + headers);
        console.log('Upload complete item->' + item);
        console.log(JSON.parse(response));
        this.words = JSON.parse(response);
    };
  }


}
